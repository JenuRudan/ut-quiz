const proxyquire = require( 'proxyquire' );
const assert     = require( 'assert' );

describe( 'The ./lib/expandNamespaces function', ()=>{

  it( 'should keep paths without namespaces unchanged', ()=>{

    const expandNamespaces = proxyquire( '../lib/expandNamespace', {
      './loadNamespaces': sinon.stub()
    } );

    const expected = './somePath';
    const actual   = expandNamespaces( expected );

    expect( actual ).to.equal( expected );

  } );
  it.skip( 'start writing tests here', ()=>{
  
  } );
  it( 'should convert path to a relative one using namespace', ()=>{

    const expandNamespaces = proxyquire( '../lib/expandNamespace', {
      './loadNamespaces': sinon.stub().returns( ['root/etc','root/bin'] )
    } );
    
    const expected = '..\\dummy\\dummy2';
    // const expectedUNIX = '../dummy/dummy2';
    const current = 'root/bin/xyx';
    const actual  = expandNamespaces( '<1>/dummy/dummy2/',current );

    expect( actual ).to.equal( expected );
  
  } );
  it( 'should produce assertion error with callerPath not found', ()=>{

    const expandNamespaces = proxyquire( '../lib/expandNamespace', {
      './loadNamespaces': sinon.stub().returns( ['root/etc','root/bin'] )
    } );

    try{

      expandNamespaces( '<1>/dummy/dummy2/' );
    
    }     catch( err ){

      expect( err ).to.be.an.instanceof( assert.AssertionError );    
      expect( err ).to.have.property( 'message','callerPath is not defined.' );
    
    }
    
  
  } );
  it( 'should produce assertion error with namespace not found', ()=>{

    const expandNamespaces = proxyquire( '../lib/expandNamespace', {
      './loadNamespaces': sinon.stub().returns( ['root/etc','root/bin'] )
    } );

    try{

      expandNamespaces( '<5>/dummy/dummy2/' );
    
    }     catch( err ){
      
      expect( err ).to.be.an.instanceof( assert.AssertionError );    
      expect( err ).to.have.property( 'message','namespace <5> is not defined.' );
    
    }
    
  
  } );
  

} );
